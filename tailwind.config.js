module.exports = {

  // future: {
  //   removeDeprecatedGapUtilities: true,
  // },
  // purge: {
  //   enabled: true,
  //   content: ['./src/*.js', './src/**/*.js', './src/**/**/*.js'],
  // },
  theme: {
    extend: {
      colors: {
        grey_primary: '#f8f8f7',
        orange_primary: '#e06b43',
        light_pink_primary: '#f6f0f0',
      },
      width: {
        '100vw' : '100vw'
      },
      height: {
        '100vh' : '100vh'
      },
    },
  },
  plugins: [],
}
