import React from 'react'
import './styles/output.css';

export default function App() {
  return (
    <div className='w-100vw h-100vh bg-grey_primary flex justify-center items-center'>
      <div className='w-2/4 h-2/3 flex rounded-3xl p-4 justify-center items-center bg-white'>
        <div className='grid h-full pt-32 w-full px-40'>
          <input className='bg-light_pink_primary h-10 px-4 py-2 w-full rounded-md' type='text' name='username' placeholder='Username' />
          <input className='bg-light_pink_primary h-10 px-4 py-2 w-full rounded-md' type='text' name='password' placeholder='Password' />

          <p className='text-right h-4 text-xs font-semibold'>Forgot password?</p>

          <button className='bg-orange_primary h-10 w-9/12 justify-self-center py-2 rounded-2xl text-white text-lg font-semibold'>Login</button>

          <p className='text-center text-xs h-4 mt-16'>Don't have an account? <span className='font-extrabold'>Create One</span> </p>
        </div>
      </div>
    </div>
  );
}

